_On me demande parfois des références et outils (à tendance libre et/ou open source) et j'ai décidé de compiler ici ceux que j'utilise le plus, que je recommande grandement pour les utiliser fréquemment (sélection donc très personnelle)._  
  
_A considérer que c'est « en travaux » pour le moment, je suis preneur de toute suggestion._  
  
_Bonne lecture !  
Franck_
<br><br>
## Systèmes d'Exploitation 🐧

#### Serveur
- [Debian](https://www.debian.org) & [YunoHost](https://yunohost.org) ( Auto-Hébergement )

#### Poste de Travail
- [Manjaro](https://manjaro.org) (avec [GNOME](https://www.gnome.org)) : L'avantage de la stabilité d'**Arch Linux** avec une prise en main plus aisée, y'a qu'à installer pour bosser 🤓
- [elementary OS](https://elementary.io) : Basée sur **Ubuntu** (sans gestionnaire de paquets _Snap_), distribution qui rappelera fortement _macOS&trade;_ sans ses verrous
- [Pop! OS](https://pop.system76.com) : Également basé sur **Ubuntu** (et toujours sans _Snap_), je l'utilise principalement pour du multimédia
- A surveiller, la jeune (mais prometteuse) distribution [GNOME OS](https://os.gnome.org) avec une expérience **GNOME** pure et l'_app store_ [Flathub](https://flathub.org) par défaut

#### Mobile
- [LineageOS](https://lineageos.org) : Version [AOSP](https://source.android.com) d'_Android&trade;_, fonctionne parfaitement sans les produits _Google&trade;_ (et avec le _store_ d'applications [F-Droid](https://f-droid.org))
- [/e/OS](https://e.foundation/fr/e-os/) : Autre dérivé d'AOSP, initié par Gaël Duval (l'un des papas de feu [MandrakeLinux](https://fr.wikipedia.org/wiki/Mandriva_Linux)), avec un écosystème complet et « déGooglisé »
- [postmarketOS](https://postmarketos.org) : Très intéressante et dynamique adaptation de **Linux** avec plusieurs interfaces possibles dont **GNOME** ou encore **Plasma**
- [Mobian](https://mobian-project.org) : Adaptation sur mobile de **Debian**, avance lentement mais surement …
- [Ubuntu Touch](https://ubuntu-touch.io) : Après l'abandon par _Canonical&trade;_, [UBports](https://ubports.com) et sa communauté ont repris de main de maître cette version mobile d'**Ubuntu**

#### Autres
- :watch:&nbsp;[AsteroidOS](https://asteroidos.org) : Excellente alternative à _Android Wear_ pour les montres connectées
- :joystick:&nbsp;[Lakka](https://lakka.tv) & [Recalbox](https://www.recalbox.com) : Distributions **Linux** dédiées à l'émulation et au _retrogaming_
- 📻&nbsp;[Volumio](https://volumio.com/en/get-started/) : Solution _jukebox_ sur réseau local, installable sur une carte _Raspberry Pi&trade;_
- :tv:&nbsp;[webOS OSE](https://www.webosose.org) : Projet que je suis, c'est le pendant _open source_ du _webOS&trade;_ racheté par _LG&trade;_
<br><br>
## Logiciels

#### Conception
- [Akira](https://github.com/akiraux/Akira) : Projet en développement d'outil de conception UX
- [Minder](https://github.com/phase1geo/Minder) : Application de cartographie mentale pour organiser et visualiser nos idées

#### Développement
- [DBeaver](https://dbeaver.io) : Gestion de base de données multiplateforme (très pratique pour la consultation à la volée)
- [Éditeur de texte GNOME](https://apps.gnome.org/fr/app/org.gnome.TextEditor) : Éditeur de texte (et plus si affinités) pour GNOME (comme indiqué) dont j'ai fait la 1<sup>ere</sup> traduction française
- [Geany](https://www.geany.org) : IDE léger et paramétrable (j'ai d'ailleurs quelques [ajouts](https://codeberg.org/DansLeRuSH/geany) à vous proposer)
- [GnuCOBOL](https://gnucobol.sourceforge.io) : Compilateur COBOL libre, qui traduit les programmes en C et les compile à l'aide d'un compilateur C natif … intéret professionnel 🤓
- [Meld](https://meldmerge.org) : Indispensable pour la comparaison de code et le _merge_
- [Notepad++](https://notepad-plus-plus.org) : Autre IDE (sous Windows) avec mon [thème sombre _DansLeRuSH-Dark_](https://codeberg.org/DansLeRuSH/notepad-plus-plus-dark-theme) inclus depuis la version 7.8.7
- [Seer](https://github.com/epasveer/seer) : Interface graphique pour [GNU Projet Debugger](https://www.sourceware.org/gdb/) (GDB)
- [VSCodium](https://vscodium.com) : Pour qui voudrait utiliser _Visual Studio Code_ avec des binaires libres et sans la "télémétrie" de _Microsoft&trade;_ 🕵️
- [Zowe](https://zowe.org) (de l'[Open Mainframe Project](https://openmainframeproject.org)) : _Framework open source_ intégré et extensible pour _Mainframe_ z/OS … aussi intéret professionnel 🤓

#### Graphisme
- [GIMP](https://gimp.org) : Éditeur _bitmap_ très complet, je l'utilise quasi quotidiennement
- [Goxel](https://goxel.xyz) : Éditeur voxel, parfait pour empiler les cubes
- [GrafX2](http://pulkomandy.tk/projects/GrafX2) : Un brin rétro mais idéal pour faire du _pixel art_ 👾  
- [Inkscape](https://inkscape.org) : Très complet (et suivi)  éditeur vectoriel, je fais tous mes _.svg_ avec
- [RawTherapee](https://rawtherapee.com) : Système de traitement de photos brutes (raw) multiplateforme
- [Upscayl](https://upscayl.org) : Utilisant des algorithmes assez avancés pour _upscaler_ les images en basse résolution

#### Internet
- [Firefox](https://www.mozilla.org/fr/firefox) : LE navigateur (imparfait certes mais le seul valable)  
- [GNOME Web (Epiphany)](https://apps.gnome.org/fr/Epiphany/) : Navigateur minimal très pratique (aussi) pour tester WebKit sous Linux
- [Thunderbird](https://thunderbird.net) : Indispensable client pour les courriels, très pratique dans sa gestion multi-comptes

#### Réseau
- [RustDesk](https://rustdesk.com) : Prise en main de bureau à distance pour Linux, macOS, Windows et Android
- [Wireshark](https://wireshark.org) : Excellent pour capturer les trames réseaux et surveiller son trafic

#### Virtualisation
- [Podman](https://podman.io) : Alternative libre à _Docker&trade;_ sans besoin de permissions ROOT (ni de daemon), permet le lancement d'applications dans des conteneurs logiciels
<br><br>
## Applications Web / Réseaux

#### Collaboration
- [Dolibarr](https://dolibarr.org) : Progiciel de gestion intégré et de relation client (ERP/CRM)
- [Mobilizon](https://joinmobilizon.org/fr/) : Organisation d'évènements et gestion de groupes
- [Nextcloud](https://nextcloud.com) : Hébergement de fichiers et outils de collaboration 

#### Communication
- [BigBlueButton](https://bigbluebutton.org) : Visioconférence développé pour la formation à distance, avec présentation de diapositives, _tableau blanc_ et sondages
- [Jitsi](https://jitsi.org) : « _Alternative open source à Zoom&trade;_ » avec messagerie instantanée, voix sur IP, visioconférence et chiffrement de bout en bout
- [Matrix](https://matrix.org) :  Standard ouvert pour communication (chat) interopérable, décentralisée et temps réel (audio/vidéo également)
- [Mattermost](https://mattermost.com) : Messagerie instantanée libre, auto-hébergeable et dédiée aux organisations ou entreprises 

#### Conception
- [Penpot](https://penpot.app) : Plate-forme de conception et de prototypage _open source_

#### Développement
- [Forgejo](https://forgejo.org) : Forge logicielle basée sur **Git**, dérivée de _Gitea&trade;_, légère, complète et à jamais communautaire

#### Multimédia
- [Audiobookshelf](https://www.audiobookshelf.org) : Serveur de livres audio et de podcasts auto-hébergé
- [Castopod](https://castopod.org) : Solution très complète d'hébergement de podcast
- [PeerTube](https://joinpeertube.org) : Du _streaming_ vidéo fédéré, en _par-à-pair_ et auto-hébergé

#### Sécurité / InfoSec
- [CrowdSec](https://www.crowdsec.net) : Fail2Ban collaboratif (identification et partage des adresses IP malveillantes)
- [Pi-Hole](https://pi-hole.net) : Blocage des publicités au niveau réseau, fonctionne comme un DNS menteur et possiblement en DHCP

#### Traduction
- [LibreTranslate](https://libretranslate.com) : Un service de traduction en ligne open-source et gratuit qui fournit une API auto-hébergeable

#### Wiki
- [XWiki](https://xwiki.com) dont l'offre [standard](https://xwiki.com/fr/offres/produits/xwiki-standard) est gratuite