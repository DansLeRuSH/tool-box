# Search Engines
A tour of freely accessible search engines
| Site | URL | Repository | Dependance |
|---|---|:---:|:---:|
| **Alexandria** | [alexandria.org](https://alexandria.org) | [GitHub](https://github.com/alexandria-org) | ❎ |
| **DuckDuckGo** | [duckduckgo.com](https://duckduckgo.com) | ❌ | _Bing_ |
| **Mojeek** | [mojeek.com](https://mojeek.com) | ❌ | ❎ |
| **Qwant** | [qwant.com](https://qwant.com) | ❌ | _Bing_ |
| **SearXNG** | _Self-Hosting_ | [GitHub](https://github.com/searxng/searxng) | _Metasearch engine_ |
| **Startpage** | [startpage.com](https://startpage.com) | ❌ | _Google_ |
| **Stract** | [stract.com](https://stract.com) | [GitHub](https://github.com/StractOrg) | ❎ |
| **Yandex** | [yandex.com](https://yandex.com) | ❌ | ❎ |
| **Yep** | [yep.com](https://yep.com) | ❌ | ❎ |
