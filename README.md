_I sometimes get asked for references and tools (free and/or open source oriented) and I decided to compile here the ones I use the most, which I highly recommend for frequent use (so very personal selection)._  
  
_Considering that this is "work in progress" for the moment, I'm open to any suggestion._  
  
_Good reading !  
Franck_
<br><br>
## Operating Systems 🐧

#### Server
- [Debian](https://www.debian.org) & [YunoHost](https://yunohost.org) ( Self-Hosting )

#### Workstation
- [Manjaro](https://manjaro.org) (with [GNOME](https://www.gnome.org)) : The advantage of **Arch Linux**'s stability with an easier handling, just install to work 🤓
- [elementary OS](https://elementary.io) : Based on **Ubuntu** (without _Snap_ package manager), a distribution that will remind a lot _macOS&trade;_ without its locks
- [Pop! OS](https://pop.system76.com) : Also based on **Ubuntu** (and still without _Snap_), I use it mainly for multimedia
- To watch, the young (but promising) [GNOME OS](https://os.gnome.org) with a pure **GNOME** experience with [Flathub](https://flathub.org) app store by default

#### Mobile
- [LineageOS](https://lineageos.org) : [AOSP](https://source.android.com) version of _Android&trade;_, works perfectly without _Google&trade;_ products (and with the [F-Droid](https://f-droid.org) application store)
- [/e/OS](https://e.foundation/e-os/) : Other AOSP variant, initiated by Gaël Duval (one of the late [MandrakeLinux](https://en.wikipedia.org/wiki/Mandriva_Linux) founders), with complete "de-Googlized" ecosystem
- [postmarketOS](https://postmarketos.org) : Very interesting and dynamic adaptation of **Linux** with several possible interfaces including **GNOME** or **Plasma**
- [Mobian](https://mobian-project.org) : Mobile adaptation of **Debian**, slowly but surely progressing …
- [Ubuntu Touch](https://ubuntu-touch.io) : After the withdrawal of _Canonical&trade;_, [UBports](https://ubports.com) and its community have nicely taken over this mobile version of **Ubuntu**

#### Others
- :watch:&nbsp;[AsteroidOS](https://asteroidos.org) : Excellent alternative to _Android Wear_ for connected watches
- :joystick:&nbsp;[Lakka](https://lakka.tv) & [Recalbox](https://www.recalbox.com) : **Linux** distributions dedicated to emulation and retrogaming
- 📻&nbsp;[Volumio](https://volumio.com/en/get-started/) : Local network jukebox solution, installable on a _Raspberry Pi&trade;_ board
- :tv:&nbsp;[webOS OSE](https://www.webosose.org) : Project I follow, it's the open source counterpart of the _webOS&trade;_ bought by _LG&trade;_
<br><br>
## Softwares

#### Design
- [Akira](https://github.com/akiraux/Akira) : UX design tool project in development
- [Minder](https://github.com/phase1geo/Minder) : Mind mapping application to organise and visualise our ideas

#### Development
- [DBeaver](https://dbeaver.io) : Cross-platform database management (very useful for on-the-fly consultation)
- [GNOME Text Editor](https://apps.gnome.org/en/app/org.gnome.TextEditor) : Text editor (and more if affinities) for **GNOME** (as indicated) of which I made the 1st French translation
- [Geany](https://www.geany.org) : Lightweight and customizable IDE (I have some [additions](https://codeberg.org/DansLeRuSH/geany) to suggest)
- [GnuCOBOL](https://gnucobol.sourceforge.io) : Free COBOL compiler, which translates programs into C and compiles them using a native C compiler … professional interest 🤓
- [Meld](https://meldmerge.org) : Indispensable for code comparison and merge
- [Notepad++](https://notepad-plus-plus.org) : Another IDE (under _Windows_) with my [_DansLeRuSH-Dark_ dark theme](https://codeberg.org/DansLeRuSH/notepad-plus-plus-dark-theme) included since version 7.8.7
- [Seer](https://github.com/epasveer/seer) : Graphical interface for [GNU Projet Debugger](https://www.sourceware.org/gdb/) (GDB)
- [VSCodium](https://vscodium.com) : For anyone who would like to use **Visual Studio Code** with _libres_ binaries and without the _Microsoft_'s "telemetry" 🕵️
- [Zowe](https://zowe.org) (of [Open Mainframe Project](https://openmainframeproject.org)) : Integrated and extensible open source framework for Mainframe _z/OS_ … also professional interest 🤓

#### Graphics
- [GIMP](https://gimp.org) : Very complete _bitmap_ editor, I use it almost daily
- [Goxel](https://goxel.xyz) : _Voxel_ editor, perfect for stacking cubes
- [GrafX2](http://pulkomandy.tk/projects/GrafX2) : A bit retro but ideal for _pixel art_ 👾
- [Inkscape](https://inkscape.org) : Very complete (and supported) _vector_ editor, I do all my _.svg_ with
- [RawTherapee](https://rawtherapee.com) : Cross-platform raw photo processing system
- [Upscayl](https://upscayl.org) : Using advanced algorithms to upscale low-resolution images

#### Internet
- [Firefox](https://www.mozilla.org/fr/firefox) : THE browser (admittedly imperfect but the only valid one)  
- [GNOME Web (Epiphany)](https://apps.gnome.org/fr/Epiphany/) : Minimal browser very useful to (also) test _WebKit_ under **Linux**
- [Thunderbird](https://thunderbird.net) : Indispensable email client, very practical in its multi-account management

#### Network
- [RustDesk](https://rustdesk.com) : Remote desktop support for _Linux_, _macOS_, _Windows_ and _Android_
- [Wireshark](https://wireshark.org) : Excellent for capturing network data and monitoring traffic

#### Virtualization
- [Podman](https://podman.io) : Libre alternative to _Docker&trade;_ without ROOT permissions (and without daemon), allows the launch of applications in containers
<br><br>
## Web / Network applications

#### Collaboration
- [Dolibarr](https://dolibarr.org) : Enterprise resource planning and customer relationship management (ERP/CRM)
- [Mobilizon](https://joinmobilizon.org/fr/) : Event organisation and group management
- [Nextcloud](https://nextcloud.com) : File hosting and collaboration tools 

#### Communication
- [BigBlueButton](https://bigbluebutton.org) : Videoconference developed for distance learning, with slide presentation, _whiteboard_ and polling
- [Jitsi](https://jitsi.org) : « _Open source alternative to Zoom&trade;_ » with instant messaging, voice over IP, video conferencing and end-to-end encryption
- [Matrix](https://matrix.org) :  Open standard for interoperable, decentralised and real-time (also audio/video) chat communication
- [Mattermost](https://mattermost.com) : Free, self-hosted instant messaging dedicated to organisations or companies 

#### Design
- [Penpot](https://penpot.app) : Open source design and prototyping platform

#### Development
- [Forgejo](https://forgejo.org) : Git-based software forge, derived from _Gitea&trade;_, lightweight, complete and forever community-based

#### Multimedia
- [Audiobookshelf](https://www.audiobookshelf.org) : Self-hosted audiobook and podcast server
- [Castopod](https://castopod.org) : Very complete podcast hosting solution
- [PeerTube](https://joinpeertube.org) : Federated, paralleling and self-hosted video streaming

#### Security / InfoSec
- [CrowdSec](https://www.crowdsec.net) : Collaborative Fail2Ban (identifying and sharing malicious IP addresses)
- [Pi-Hole](https://pi-hole.net) : Ad blocking at network level, works as a DNS liar and possibly in DHCP

#### Translation
- [LibreTranslate](https://libretranslate.com) : An open-source and free online translation service that provides a self-hosting API

#### Wiki
- [XWiki](https://xwiki.com), whose [standard offer](https://xwiki.com/fr/offres/produits/xwiki-standard) is free